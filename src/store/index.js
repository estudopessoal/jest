import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { news } from '../reducers'

export default createStore(
  news,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunkMiddleware)
)