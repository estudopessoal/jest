import React from 'react'
import ReactDOM from 'react-dom'
import Enzyme, { mount, render, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

import Header from './Header'
import { routerList } from '../../services/routes'

describe('itens do menu', () => {
  
  let component
  beforeAll(() => {
    component = shallow(<Header/>)
  })

  it('deve ser igual ao número de objetos literais do serviço de rotas contendo a chave { header:true }', () => {
    expect(component.find('ul').children())
      .toHaveLength(
        routerList
          .filter(route => route.header === true)
          .length
      )
  })

})