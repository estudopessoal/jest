import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { routerList } from '../../services/routes'

export default class Header extends Component {
  render () {
    return (
      <div className="header">
        <ul>
          { 
            routerList
              .filter(route => route.header === true)
              .map((route, index) => 
                <li key={index}>
                  <Link to={route.path}>{route.title}</Link>
                </li>
              )
          }
        </ul>
      </div>
    )
  }
}