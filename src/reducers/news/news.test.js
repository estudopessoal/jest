import { news } from './news'

describe('action.type', () => {
  
  let state
  let action = {
    type: ''
  }

  it('vazio deve retornar o state', () => {
    state = ['eu sou o state']
    expect(news(state, action)).toEqual(state)
  })

  it('LISTAGEM deve retornar o payload news da action', () => {
    action = { type: 'LISTAGEM', news: 'teste' }
    expect(news(state, action)).toEqual(action.news)
  })

})