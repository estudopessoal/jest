import React from 'react'
import ReactDOM from 'react-dom'
import Enzyme, { mount, render, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Sobre from './Sobre'

Enzyme.configure({ adapter: new Adapter() })

describe('<Sobre/>', () => {

  let component
  beforeAll(() => {
    component = shallow(<Sobre/>)
  })

  it('deve ter o texto correto', () => {
    expect(component.text()).toContain('Eu sou a Sobre')
  })

})