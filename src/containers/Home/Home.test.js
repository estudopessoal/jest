import React from 'react'
import ReactDOM from 'react-dom'
import Enzyme, { mount, render, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Home from './Home'

Enzyme.configure({ adapter: new Adapter() })

describe('this.state.news', () => {

  let component
  beforeAll(() => {
    component = shallow(<Home/>)
  })

  it('deve estar preenchido com 5 valores', () => {
    expect(component.state().news).toHaveLength(5)
  })

  it('deve ter chaves obrigatórias', () => {
    component.state().news.map(news => {
      expect(news.title).toBeDefined()
      expect(news.content).toBeDefined()
    })
  })

  it('deve ter todas as chaves obrigatórias preenchidas', () => {
    component.state().news.map(news => {
      expect(news.title).toBeTruthy()
      expect(news.content).toBeTruthy()
    })
  })

})