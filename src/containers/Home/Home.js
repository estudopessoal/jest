import React, { Component } from 'react'
import { news } from '../../services/api'
import store from '../../store'

export default class Home extends Component {

  constructor() {
    super()
    this.state = {
      news: []
    }
  }

  componentWillMount() {
    store.subscribe(() => this.setState({news: store.getState()}))
  }
  
  componentDidMount() {
    store.dispatch(news.lista())
  }

  render () {
    return (
      <div className="home">
        <ul>
          { this.state.news.map((news, index) => 
            <div key={ index }>
              <h1>{ news.title }</h1>
              <p>{ news.content }</p>
            </div>
          )}
        </ul>
      </div>
    )
  }
}