import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { routerList } from '../../services/routes'
import Header from '../../components/Header/Header'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="app">
          <Header/>
          <Switch>
            { routerList.map((config, index) => 
              <Route key={index} {...config}/>
            )}
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App