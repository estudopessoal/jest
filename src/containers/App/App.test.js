import React from 'react'
import ReactDOM from 'react-dom'
import Enzyme, { mount, render, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

import App from './App'
import Header from '../../components/Header/Header'

describe('<Header/>', () => {
  
  let component
  beforeAll(() => {
    component = shallow(<App/>)
  })

  it('deve existir', () => {
    expect(component.contains(<Header/>)).toBe(true)
  })

})