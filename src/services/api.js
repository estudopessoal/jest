export class news {
  static lista() {
    return dispatch => {
      
      new Promise(resolve => {
        const res = [{ title: 'news 5', content: 'lorem ipsum' }, { title: 'news 4', content: 'lorem ipsum' }, { title: 'news 3', content: 'lorem ipsum' }, { title: 'news 2', content: 'lorem ipsum' }, { title: 'news 1', content: 'lorem ipsum'}]
        resolve(res)
      }).then(news => {
        dispatch({type: 'LISTAGEM', news})
      })
 
    }
  }
}