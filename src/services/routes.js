import Home from '../containers/Home/Home'
import Sobre from '../containers/Sobre/Sobre'

export const routerList = [
  {
    title: 'Home',
    path: '/',
    component: Home,
    exact: true,
    header: true
  },
  {
    title: 'Sobre',
    path: '/sobre',
    component: Sobre,
    header: true
  },
  {
    title: 'Sobre2',
    path: '/sobre2',
    component: Sobre
  }
]